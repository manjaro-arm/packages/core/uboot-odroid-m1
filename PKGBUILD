# U-Boot: Odroid M1 (Mainline with Rockchip blobs)
# Maintainer: Dan Johansen <strit@manjaro.org>

pkgname=uboot-odroid-m1
pkgver=2022.04.rc1
pkgrel=1
#_tfaver=2.5
pkgdesc="U-Boot for Odroid M1"
arch=('aarch64')
url='http://www.denx.de/wiki/U-Boot/WebHome'
license=('GPL')
makedepends=('dtc' 'bc' 'uboot-tools' 'python-setuptools' 'swig')
provides=('uboot')
conflicts=('uboot')
install=${pkgname}.install
source=( #"ftp://ftp.denx.de/pub/u-boot/u-boot-${pkgver/rc/-rc}.tar.bz2" #for mainline release
        "https://gitlab.com/pgwipeout/u-boot-quartz64/-/archive/quartz64/u-boot-quartz64-quartz64.tar.gz" #Peter Geis' u-boot based on Mainline
        "rk3568_bl31_v1.28.elf::https://github.com/JeffyCN/rockchip_mirrors/blob/6186debcac95553f6b311cee10669e12c9c9963d/bin/rk35/rk3568_bl31_v1.28.elf?raw=true"
        "rk3568_ddr_1560MHz_v1.13.bin::https://github.com/JeffyCN/rockchip_mirrors/blob/ddf03c1d80b33dac72a33c4f732fc5849b47ff99/bin/rk35/rk3568_ddr_1056MHz_v1.13.bin?raw=true"
        "boot.cmd")
        #"https://git.trustedfirmware.org/TF-A/trusted-firmware-a.git/snapshot/trusted-firmware-a-$_tfaver.tar.gz") #for TF-A release
sha256sums=('2d1dcaa3dfdd7684e69402a9cd73440c5ab4dfddb15e0c05178da3a5b415647c'
            '67bf19566fb646e2f1f55b7fbf084f0d71b59b875a19a077e638b95adf1b254a'
            '6f165b37640eb876b5f41297bcce6451eb8a86fa56649633d4aca76047136a36'
            '2e33ada0b4ddb1f6223990978de5cfd8a82f9cfc05e854558a1c090a620ca2ba')

prepare() {
  mv rk3568_bl31_v1.28.elf u-boot-quartz64-quartz64/bl31.elf
  mv rk3568_ddr_1560MHz_v1.13.bin u-boot-quartz64-quartz64/ram_init.bin
  #cd u-boot-${pkgver/rc/-rc}
}

build() {
  # Avoid build warnings by editing a .config option in place instead of
  # appending an option to .config, if an option is already present
  update_config() {
    if ! grep -q "^$1=$2$" .config; then
      if grep -q "^# $1 is not set$" .config; then
        sed -i -e "s/^# $1 is not set$/$1=$2/g" .config
      elif grep -q "^$1=" .config; then
        sed -i -e "s/^$1=.*/$1=$2/g" .config
      else
        echo "$1=$2" >> .config
      fi
    fi
  }

  unset CFLAGS CXXFLAGS CPPFLAGS LDFLAGS
  
  #echo -e "\nBuilding TF-A for Pine64 Quartz64...\n"
  #cd trusted-firmware-a-$_tfaver
  #make PLAT=rk356x
  #cp build/rk356x/release/bl31/bl31.elf ../u-boot-${pkgver/rc/-rc}/
  
  #cd ../u-boot-${pkgver/rc/-rc}
  cd u-boot-quartz64-quartz64

  echo -e "\nBuilding U-Boot for Hardkernel Odroid M1...\n"
  # No odroid-m1 defconfig yet, but quartz64 one boots
  make quartz64-b-rk3566_defconfig

  update_config 'CONFIG_IDENT_STRING' '" Manjaro Linux ARM"'
  make EXTRAVERSION=-${pkgrel}
}

package() {
  #cd u-boot-${pkgver/rc/-rc}
  cd u-boot-quartz64-quartz64

  mkdir -p "${pkgdir}/boot/extlinux"

  install -D -m 0644 idbloader.img u-boot.itb -t "${pkgdir}/boot"
  install -D -m 0644 "${srcdir}/boot.cmd" -t "${pkgdir}/boot"
  mkimage -T script -A arm64 -C none -d "${pkgdir}/boot/boot.cmd" "${pkgdir}/boot/boot.scr"
}
